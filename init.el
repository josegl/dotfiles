
;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

(require 'package)
(add-to-list 'package-archives
             '("melpa" . "http://melpa.org/packages/") t)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   (quote
    ("aaffceb9b0f539b6ad6becb8e96a04f2140c8faa1de8039a343a4f1e009174fb" default)))
 '(package-selected-packages
   (quote
    (editorconfig indium expand-region dracula-theme multiple-cursors magit))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; GUI tweaks ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Disabe the toolbar. Seriosly. Who had the great idea to have an icon bar here. Is this Gimp?
(tool-bar-mode -1)
;; Disabe the menu bar. Who the hell would want to have a graphic menu in this kind of program?
(menu-bar-mode -1)
;; Disabe the scroll bar. In fact this does not hurt, but having the % of the file you are reading is enough.
(scroll-bar-mode -1)
;; Set the theme to dracula
(load-theme 'dracula t)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Custom shortcuts ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Init magit-status to C-x g
(global-set-key (kbd "C-x g") 'magit-status)
;; Expand-region configuration
(global-set-key (kbd "C-=") 'er/expand-region)
